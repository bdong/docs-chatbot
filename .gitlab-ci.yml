image: nimmis/java-centos:openjdk-8-jre

## Install some pre-requisites
before_script:
  - yum makecache fast
  - yum install which -y

## Global variables that can be used in different jobs
variables:
  REVIEW_SITE_BASE_URL: "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"

## Pipeline stages that are executed in sequence
stages:
  - build
  - test
  - deploy

## Master branch build
build-master:
  stage: build
  script:
    - cd root
    - ./gradlew clean build
  artifacts:
    when: on_success
    expire_in: 3 weeks
    paths:
      - website/build/${CI_PROJECT_NAME}
  only:
    - master

## Master branch test
test-master:
  stage: test
  script:
    - cd root
    - ./gradlew clean test
  only:
    - master

## Non-master branch build
build-branch:
  stage: build
  script:
    - cd root
    - ./gradlew clean build -PsiteBaseUrl="${REVIEW_SITE_BASE_URL}"
  artifacts:
    when: on_success
    expire_in: 2 weeks
    paths:
      - website/build/${CI_PROJECT_NAME}
  except:
    - master
  only:
    - branches

## Non-master branch test
test-branch:
  stage: test
  script:
    - cd root
    - ./gradlew clean test
  except:
    - master
  only:
    - branches

## Deployment: review site Non-master branch
deploy-review-site:
  stage: deploy
  image: garland/aws-cli-docker:1.15.47
  variables:
    GIT_STRATEGY: none
    AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID_REVIEW}
    AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY_REVIEW}
  dependencies:
    - build-branch
  before_script:
    # overwrite the global "before_script" as we don't need it for this job
    - echo "Start deploying review site"
  script:
    - cd website/build/${CI_PROJECT_NAME}
    - aws s3 sync . ${AWD_S3_BUCKET_URL_REVIEW}/${REVIEW_SITE_BASE_URL} --delete
    - aws cloudfront create-invalidation --distribution-id ${AWD_CLOUDFRONT_DISTID_REVIEW} --paths /${REVIEW_SITE_BASE_URL}/* 
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    url: "https://review-docs.elasticpath.com/${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}/"
    on_stop: remove-review-site
  except:
    - master
  only:
    - branches

remove-review-site:
  stage: deploy
  image: garland/aws-cli-docker:1.15.47
  variables:
    GIT_STRATEGY: none
    AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID_REVIEW}
    AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY_REVIEW}
  before_script:
    # overwrite the global "before_script" as we don't need it for this job
    - echo "Start removing review site"
  script:
    - aws s3 rm ${AWD_S3_BUCKET_URL_REVIEW}/${REVIEW_SITE_BASE_URL} --recursive
    - aws cloudfront create-invalidation --distribution-id ${AWD_CLOUDFRONT_DISTID_REVIEW} --paths /${REVIEW_SITE_BASE_URL}/*
  environment:
    name: review/${CI_COMMIT_REF_NAME}
    action: stop
  when: manual
  except:
    - master
  only:
    - branches

## Deployment: staging site for the master branch after changes have been merged
deploy-staging-site:
  stage: deploy
  image: garland/aws-cli-docker:1.15.47
  variables:
    GIT_STRATEGY: none
    AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID_STAGING_PROD}
    AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY_STAGING_PROD}
  dependencies:
    - build-master
  before_script:
    # overwrite the global "before_script" as we don't need it for this job
    - echo "Start deploying staging site"
  script:
    - cd website/build/${CI_PROJECT_NAME}
    - aws s3 sync . ${AWD_S3_BUCKET_URL_STAGING}/chatbot --delete
    - aws cloudfront create-invalidation --distribution-id ${AWD_CLOUDFRONT_DISTID_STAGING} --paths /chatbot/* 
  environment:
    name: staging
  only:
    - master

## Deployment: production site for the master branch
deploy-production-site:
  stage: deploy
  image: garland/aws-cli-docker:1.15.47
  variables:
    GIT_STRATEGY: none
    AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID_STAGING_PROD}
    AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY_STAGING_PROD}
  dependencies:
    - build-master
  before_script:
    # overwrite the global "before_script" as we don't need it for this job
    - echo "Start deploying production site"
  script:
    - cd website/build/${CI_PROJECT_NAME}
    - aws s3 sync . ${AWD_S3_BUCKET_URL_PRODUCTION}/chatbot --delete
    - aws cloudfront create-invalidation --distribution-id ${AWD_CLOUDFRONT_DISTID_PRODUCTION} --paths /chatbot/* 
  environment:
    name: production
  when: manual
  only:
    - master
