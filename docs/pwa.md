---
id: pwa
title: Progressive Web Application
sidebar_label: PWA
---

This section of the guide provides information on interactions with the Chatbot, the usages of the chatbot for standalone PWAs (Progressive Web Applications), and its integration with the [React PWA Reference Storefront](https://github.com/elasticpath/react-pwa-reference-storefront/).

The Chatbot PWA makes use of the [Amazon Amplify](https://aws-amplify.github.io/docs/) Chatbot component as the front-end to the Lex Conversational service. This same component may be used in a variety of front-end web applications, and is used for the React PWA Reference Storefront.

## Using the Chatbot PWA

### Start the chatbot PWA

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/lex-chatbot.git

# Go into the cloned directory
cd lex-chatbot/ep-pwa-chatbot

# Install all the dependencies for the project
npm install

# Start the app in development mode

# Run the main application:
npm start
```

To deploy the chatbot PWA for production, see [Deploy the Chatbot PWA with Amplify](./examples/amplify-deploy.md).

The Chatbot operates under two shopper types - public, and registered.

### Public User

In the Chatbot PWA window, typing `Hello ep` will initialize the Chatbot. This will request a public token for the chatbot to interact with Cortex.

### Registered User

The Chatbot server accepts a Cortex bearer token passed through the intent as a slot under the type `AuthToken` with name `token`, which will be set as a session attribute, and will propagate it to the lambda to perform actions on behalf of the authenticated user. This token is intended to be passed through the intent and then served as a session attribute through the Amazon Amplify framework. If a token is provided, the `Hello EP` intent is not required to be executed.

The utterrance to be invoked is `ep-auth {token}`, where `token` is the slot for the bearer token which will be used for the session.

<!-- ## Using the Chatbot React PWA Reference Storefront

To configure the chatbot for the React PWA Reference Storefront, see [Setup the chatbot with the React PWA Reference Storefront](./examples/storefront.md).

The Chatbot is only intended to operate under the shopper registered shopper type.

### Registered User

The Chatbot server accepts a Cortex bearer token passed through the intent as a slot under the type `AuthToken` with name `token`, which will be set as a session attribute, and will propagate it to the lambda to perform actions on behalf of the authenticated user. This token is intended to be passed through the intent and then served as a session attribute through the Amazon Amplify framework. If a token is provided, the `Hello EP` intent is not required to be executed.

The utterrance to be invoked is `ep-auth {token}`, where `token` is the slot for the bearer token which will be used for the session. -->
