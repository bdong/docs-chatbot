---
id: setup-storefront
title: Setup the chatbot with the React PWA Reference Storefront
sidebar_label: Setup the chatbot with the React PWA Reference Storefront
---

The React PWA (Progressive Web Application) Reference Storefront is developed to integrate with the Reference Chatbot.

## Prerequisites

1. Ensure that an AWS (Amazon Web Services) account is created
2. Ensure that an AWS Identity and Access Management (IAM) profile is created
3. Ensure that the AWS CLI tool is installed
For more information about platform specific procedures, see the [Amazon documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
4. Ensure that you set up your AWS credentials on CLI
    * **Note:** Open a new terminal, and run `aws configure`
5. Ensure that [AWS Amplify CLI](https://aws-amplify.github.io/docs/) tool is installed
6. Ensure that you have completed the instructions to setup the Chatbot in [Setting up Chatbot server](chatbot-server.html)

## Chatbot Component with the React PWA Reference Storefront Example

1. In the `./react-pwa-reference-storefront/app/src/ep.config.json` file, update the `chatbot.enable` to **true**
2. In the `./react-pwa-reference-storefront/app/src/ep.config.json` file, update the `chatbot.name` parameter with the name of the chatbot you have previously setup
3. Update the `chatbot-welcome-msg` string in `./react-pwa-reference-storefront/app/localization/en-CA.json` file as desired. By default,  `en-CA` and `fr-FR` locales are provided in the project
4. Go to the `react-pwa-reference-storefront/app` directory
5. Run `amplify init`. You can accept all default values and configure if necessary
6. Run `amplify add interactions` to create Lex Model hooks.
    1. Step through the creation wizard and choose **Use a Sample Chatbot**
    2. Once the example bot is created, run `amplify push` to generate an `aws-exports.js` folder in your source directory
7. In the following files, change the BotName field to the EPConversationalBot on your AWS account:
    * `./react-pwa-reference-storefront/app/src/aws-exports.js` - `aws_bots_config.name = "[Your Bot Name]"`
    * `./react-pwa-reference-storefront/app/src/App.js` - `botName = "[Your Bot Name]"`

After completing the setup, you can access your storefront and see that chatbot component loaded upon logging into the store.

![Chatbot Storefront](assets/chatbot-storefront.png)
