---
id: response-cards
title: Displaying Product Images in Response Cards
sidebar_label: Configure Response Cards
---

You can configure Chatbot Lambda to return product images as attachments for the response cards in the response from Amazon Lex. These images can be used with the front-end touchpoints, such as Facebook, Slack, or Twilio, that can render response cards directly within the chat interface.
For more information about using response cards in Lex, see the [Amazon documentation](https://docs.aws.amazon.com/lex/latest/dg/ex-resp-card.html)

## Prerequisites

1. Ensure that you have uploaded all your catalog images to a CDN (Content Delivery Network) of your choice, each named as the represented sku
2. Ensure that your Lex Chatbot is deployed and the setup is completed as in the [Setting up the Chatbot Server](../setup.md) section

## Displaying Product Images in Chatbot Example

1. In the Amazon Lambda Console, locate your function called `EPConversationalLambda`
2. In the **Environment Variables** field, configure the `SKU_IMAGES_URL`setting and save the function

    `SKU_IMAGES_URL`: Specifies the path to catalog images hosted on an external CDN. Set this to the complete URL path to your hosted image directory. The lambda function will append the `sku.png` fetched from cortex onto the end this URL.

    For example:
    | Key            | Value                                |
    | -------------- | ------------------------------------ |
    | `SKU_IMAGES_URL`     | `https://s3-us-west-2.amazonaws.com/ep-demo-images/VESTRI_VIRTUAL/`  |

With these settings, the responses from Lex pertaining to product lookups returns responseCards, with the corresponding image URLs provided as in the following example:

```json
{
  "dialogState": "Fulfilled",
  "intentName": "KeywordSearchIntent",
  "message": "Okay. I found 1 result for mad max. The first result is: \"Mad Max Bundle\"",
  "messageFormat": "PlainText",
  "responseCard": {
    "contentType": "application/vnd.amazonaws.card.generic",
    "genericAttachments": [
      {
        "attachmentLinkUrl": null,
        "buttons": [
          {
            "text": "Add to cart",
            "value": "Add it to my cart"
          }
        ],
        "imageUrl": "https://s3-us-west-2.amazonaws.com/elasticpath-demo-images/VESTRI_VIRTUAL_TMP/78547.png",
        "subTitle": "$754.80",
        "title": "Mad Max Bundle"
      }
    ],
    "version": "1"
  },
```

After completing the set up, you can test your chatbot in the Amazon Lex test interface. This interface is capable of rendering the response cards, which contain URLs to your product images. Response cards may be interacted with to perform actions.

![Chatbot Response Cards](assets/chatbot-response-cards.png)
